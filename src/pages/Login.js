import { useState, useEffect, useContext } from "react";

import { Navigate } from "react-router-dom";
import { Form, Button } from "react-bootstrap";
import Swal from "sweetalert2";

import UserContext from "../UserContext";


export default function Login() {

    // Allow us to consume the User Context object and its values(properties) to use for user validation.
    const {user, setUser} = useContext(UserContext);

    // State hooks ...
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    // Allow us to gain access to methods that allow us to redirect to another page
    // const navigate = useNavigate();


    console.log(email);
    console.log(password);


    useEffect(() => {
        // Validation to enable submit button when all fields are populated
        if (email !== '' && password !== ''){
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    });

    function authenticate(e) {

        // Prevents page loading / redirection via form submission.
        e.preventDefault();

        /* 
            Syntax:
                fetch("url", {options})
                .then(res => res.json())
                .then(data => {})

                /users
                /courses
        */

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data.accessToken);
            if(data.accessToken !== undefined){
                // The JWT token...
                localStorage.setItem("token", data.accessToken);
                retrieveUserDetails(data.accessToken);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                });
            } else {
                Swal.fire({
                    title: "Authentication Failed!",
                    icon: "error",
                    text: "Check your login details and try again."
                });
            }
        })


        // "localStorage" is a property that allows JavaScript sites and application to save key-value pairs in a web browser with no expiration date.
        // Syntax:
            // localStorage.setItem("propertyName", value);

        // localStorage.setItem("email", email);

        // setUser({
        //     email: localStorage.getItem("email")
        // });

        // Clear input fields
        setEmail('');
        setPassword('');

        // Notify user for registration
        // alert(`${email} has been verified! Welcome back!`);

        // Redirect us to home page
        // navigate("/");
    };

    
    const retrieveUserDetails = (token) => {
        // Token will be sent as part of the request's header.

        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            // Change the global "user" state to store the "id" and "isAdmin" property.
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }


    return (
        // Create a conditional statement that will redirect the user to the course page when a user is already logged in.
        (user.id !== null)
        ?
            <Navigate to="/courses" />
        :
            <>
                <h1 className="my-5">Login</h1>
                <Form onSubmit={e => authenticate(e)}>
                    <Form.Group className="mb-3" controlId="email">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Enter email"
                            value={email}
                            onChange={e => setEmail(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            type="password"
                            placeholder="Password"
                            value={password}
                            onChange={e => setPassword(e.target.value)}
                            required
                        />
                    </Form.Group>


                    {/* Conditional rendering - submit button will be active based on the isActive state */}
                    {
                        isActive
                            ?
                            <Button
                                variant="success"
                                type="submit"
                                id="submitBtn">
                                Login
                            </Button>

                            :
                            <Button
                                variant="danger"
                                type="submit"
                                id="submitBtn"
                                disabled>
                                Login
                            </Button>
                    }
                </Form>
            </>
    )
};