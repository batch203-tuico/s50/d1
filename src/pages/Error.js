import Banner from "../components/Banner";

/* 
    s53 Activity Instructions:
    1. Create a route which will be accessed when a user enters an undefined route and display the Error page.
    2. Refactor the Banner component to be reusable for the Error page and the Home page.
    a. Create a object variable named data that will contain the following properties:
    - title
    - description
    -  destination
    - label
    b. Use the data variable as prop for the Banner that will be passed from the parent component (Home and Error page).
    3. Update the local repository, and push to git with the commit message of Add activity code - S53.
4. Add the link in Boodle.
*/



export default function Error() {

    const data = {
        title: "404 - Not Found",
        description: "The page you are looking for cannot be found",
        destination: "/",
        label: "Back Home"
    };

    return (
        <Banner bannerProp={data} />
    )
};