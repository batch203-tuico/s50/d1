// React hooks
import { useState, useEffect } from "react";

import { Card, Button, Carousel } from "react-bootstrap";
import { Link } from "react-router-dom";

/*
Mini Activity

    1. Create a "CourseCard" component showing a particular course with the name, description and price inside a React-Bootstrap Card.
        - Import the "Card" and "Button" in the react-bootstrap.
        - The "Card.Body" should contain the following:
            - The course name should be in the "Card.Title".
            - The "description" and "price" label should be in the "Card.Subtitle".
            - The value for "description" and "price" should be in the "Card.Text".
            - Add a "Button" with "primary" color for the Enroll.
    2. Create a "Courses.js" page and render the "CourseCard" component inside of it.
    3. Render also the "Courses" page in the parent component to mount/display it in our browser.
    4. Take a screenshot of your browser and send it in the batch hangouts

*/

                            // destructure the courseProp from the prop parameter
                            // CourseCard(prop) - before
export default function CourseCard({courseProp}){

    // console.log(props.courseProp.name);
    // console.log(typeof props);
    // console.log(courseProp);

    // Scenario: Keep track the number of enrollees of each course.

    // Destructure the course properties into their own variables
    const { _id, name, description, price, slots } = courseProp;

    // Syntax (useState):
        // const [stateName, setStateName] = useState(initialStateValue);
            // Using the state hook, it return an array with the following elements:
                // first element contains the current initial State value
                // second element is a function that is used to change the value of the first element.

        // const [count, setCount] = useState(0);
        // console.log(useState(10));

/*
    s55 Activity:
    1. Create a seats state in the CourseCard component and set the initial value to 30.
    2. For every enrollment, deduct one to the seats.
    3. If the seats reaches zero do the following:
    - Do not add to the count.
    - Do not deduct to the seats.
    - Show an alert that says No more seats available.
    4. Update your remote repository.
    5. Add another the remote link and push to git with the commit message of Add activity code - S51.
    6. Add the link in Boodle.

*/
        // const [seats, setSeats] = useState(30);

        // Function that keeps track enrollees for a course

        /* 
            We will refactor the "enroll" function using the "useEffect" hooks to disable the enroll button when the seats reach zero.

        */

        // Use the disabled state to disable the enroll button
        // const [disabled, setDisabled] = useState(false);

        // Syntax:
            // useEffect(function, [dependencyArray])

        // No dependency passed
            // If the useEffect() does not have a dependency array, it will run on initial render and whenever a state is set by its function.
        // useEffect(() => {
        //     console.log("Will run on initial render or on every changes with our components");
        // });


        // An empty array
            // If the useEffect() has dependency array but it is empty, it will only run on the initial render.
        // useEffect(() => {
        //     console.log("Will only run on initial render.")
        // }, []);

        // With dependency array (props or state values);
        //     // If the useEffect() has a dependency array and there is state or data in it, the useEffect will run whenever that state is updated.
        // useEffect(() => {
        //     console.log("Will run on initial render and every change on the dependency value.");
        // }, [seats]);

        // function unEnroll() {
        //     setCount(count - 1);
        // };

        // function enroll() {

                // Activity solution
        //     if (seats == 0 && count == 30){
        //         return alert(`No more seats.`);
        //     } else {
        //                 // 0 + 1
        //         setCount(count + 1);
        //         console.log(`Enrollees: ${count}`);

        //         setSeats(seats - 1)
        //         console.log(`Seats: ${seats}`)
        //     }

                        // 0 + 1
        //         setCount(count + 1);
        //         console.log(`Enrollees: ${count}`);

        //         setSeats(seats - 1)
        //         console.log(`Seats: ${seats}`)

        // };

        // useEffect(() => {
        //     if (seats <= 0){
        //         setDisabled(true);
        //         alert(`No more seats available.`);
        //     }
        // }, [seats]);


       

    return (
        <Card className="my-3">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Card.Subtitle>Slots</Card.Subtitle>
                <Card.Text>{slots} available</Card.Text>
                <Button as={Link} to={`/courses/${_id}`} variant="primary">Details</Button>
                {/* <Button variant="danger" className="ms-2" onClick={unEnroll}>Unenroll</Button> */}
            </Card.Body>
        </Card>
        
    )
};